import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  filesToUpload: Array<File> = [];
  files: Array<File>;
  listItem: string;
  list: Array<any>;
  completedList: Array<any>;
  constructor() {
    this.list = [
      {item: 'Sample Item', completed: false}
    ];
    this.completedList = [
      {item: 'Sample Completed', completed: true }
    ];
  }
  action(listType, itemId) {
    if (listType === 'list') {
      this.completedList.push(this.list[itemId]);
      this.list.splice(itemId, 1);
    } else if (listType === 'completedList') {
      this.list.push(this.completedList[itemId]);
      this.completedList.splice(itemId, 1);
    }
  }
  removeItem(listType, itemId) {
    if (listType === 'list') {
      this.list.splice(itemId, 1);
    } else if (listType === 'files') {
      this.files.splice(itemId, 1);
    } else if (listType === 'list') {
      this.completedList.splice(itemId, 1);
    }
  }
  addListItem() {
    if (this.listItem) {
      this.list.push({item: this.listItem, completed: false});
    }
    this.listItem = undefined;
    console.log(this.files);
    if (this.files.length) {
      const formData: any = new FormData();
      for (let i = 0; i < this.files.length; i++) {
        formData.append('uploads[]', this.files[i], this.files[i]['name']);
      }
      // Todo have to Import http and setup api call properly
      // this.http.post('http://localhost:3000/upload', formData)
      //     .map(result => result.json())
      //     .subscribe(result => console.log('files', result));
    }
  }
  chooseFile(fileInput) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      this.filesToUpload = <Array<File>>fileInput.target.files;
      this.files =  this.filesToUpload;
    }
  }
}
